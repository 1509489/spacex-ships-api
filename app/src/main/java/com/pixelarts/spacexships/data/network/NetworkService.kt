package com.pixelarts.spacexships.data.network

import com.pixelarts.spacexships.data.models.ShipsResponse
import retrofit2.Response
import retrofit2.http.GET

interface NetworkService {
    @GET("ships")
    suspend fun getShips():Response<List<ShipsResponse>>
}