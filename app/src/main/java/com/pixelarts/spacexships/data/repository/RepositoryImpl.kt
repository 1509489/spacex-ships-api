package com.pixelarts.spacexships.data.repository

import com.pixelarts.spacexships.data.models.ShipsResponse
import com.pixelarts.spacexships.data.network.NetworkService
import retrofit2.Response

class RepositoryImpl(private val networkService: NetworkService) : Repository {
    override suspend fun getShips(): Response<List<ShipsResponse>> = networkService.getShips()
}