package com.pixelarts.spacexships.data.repository

import com.pixelarts.spacexships.data.models.ShipsResponse
import retrofit2.Response

interface Repository {
    suspend fun getShips(): Response<List<ShipsResponse>>
}