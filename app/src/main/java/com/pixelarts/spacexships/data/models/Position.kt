package com.pixelarts.spacexships.data.models


data class Position(
    val latitude: Double,
    val longitude: Double
)