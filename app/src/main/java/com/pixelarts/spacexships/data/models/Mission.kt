package com.pixelarts.spacexships.data.models


data class Mission(
    val name: String,
    val flight: Int
)