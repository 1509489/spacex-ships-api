package com.pixelarts.spacexships.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.pixelarts.spacexships.AppController
import com.pixelarts.spacexships.R
import com.pixelarts.spacexships.data.models.ShipsResponse
import com.pixelarts.spacexships.di.ActivityModule
import com.pixelarts.spacexships.epoxy.ShipController
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject lateinit var viewModel: MainViewModel

    private lateinit var shipController: ShipController
    private lateinit var ships: List<ShipsResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val appComponent = AppController().appComponent
        appComponent.newActivityScope(ActivityModule(this))
            .inject(this)

        viewModel.showShips().observe(this, Observer {
            ships = it

            shipController = ShipController(ships)
            recyclerView.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = shipController.adapter
                addItemDecoration(DividerItemDecoration(this@MainActivity, LinearLayoutManager.VERTICAL))
            }
            shipController.requestModelBuild()
        })

    }
}
