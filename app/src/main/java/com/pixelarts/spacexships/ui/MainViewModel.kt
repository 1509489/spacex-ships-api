package com.pixelarts.spacexships.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pixelarts.spacexships.data.models.ShipsResponse
import com.pixelarts.spacexships.data.repository.Repository
import kotlinx.coroutines.launch

class MainViewModel(private val repository: Repository): ViewModel() {

    private val shipsLiveData = MutableLiveData<List<ShipsResponse>>()

    fun showShips():LiveData<List<ShipsResponse>>{
        viewModelScope.launch {
            shipsLiveData.value = getShips().body()
        }
        return shipsLiveData
    }

    private suspend fun getShips() = repository.getShips()
}