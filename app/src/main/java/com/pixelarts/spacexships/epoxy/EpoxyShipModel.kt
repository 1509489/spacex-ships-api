package com.pixelarts.spacexships.epoxy

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.Glide
import com.pixelarts.spacexships.R
import com.pixelarts.spacexships.data.models.ShipsResponse
import kotlinx.android.synthetic.main.ships_layout.view.*

@EpoxyModelClass(layout = R.layout.ships_layout)
abstract class EpoxyShipModel(@EpoxyAttribute var ship: ShipsResponse):EpoxyModelWithHolder<EpoxyShipModel.ShipHolder>() {

    override fun bind(holder: ShipHolder) {
        Glide.with(holder.itemView.context)
            .load(ship.image)
            .into(holder.shipImage)

        holder.shipName.text = ship.shipName
        holder.shipType.text = ship.shipType
    }

    inner class ShipHolder:EpoxyHolder(){
        lateinit var itemView: View
        lateinit var shipImage: ImageView
        lateinit var shipName: TextView
        lateinit var shipType: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            shipImage = itemView.ivImage
            shipName = itemView.tvName
            shipType = itemView.tvType
        }
    }
}