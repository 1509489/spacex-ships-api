package com.pixelarts.spacexships.epoxy

import com.airbnb.epoxy.EpoxyController
import com.pixelarts.spacexships.data.models.ShipsResponse

class ShipController(private val ships: List<ShipsResponse>): EpoxyController(){
    private var id = 0L

    override fun buildModels() = ships.forEach {
        EpoxyShipModel_(it)
            .id(id++)
            .addTo(this)
    }
}