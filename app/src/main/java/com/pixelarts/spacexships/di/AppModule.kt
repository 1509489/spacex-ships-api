package com.pixelarts.spacexships.di

import com.pixelarts.spacexships.data.network.NetworkService
import com.pixelarts.spacexships.data.repository.Repository
import com.pixelarts.spacexships.data.repository.RepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @AppScope
    fun providesRepository(networkService: NetworkService):Repository = RepositoryImpl(networkService)
}