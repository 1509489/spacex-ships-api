package com.pixelarts.spacexships.di

import dagger.Component

@AppScope
@Component(modules = [NetworkModule::class, AppModule::class])
interface AppComponent {
    fun newActivityScope(activityModule: ActivityModule):ActivityComponent
}