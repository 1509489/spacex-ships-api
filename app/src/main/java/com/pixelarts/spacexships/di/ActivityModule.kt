package com.pixelarts.spacexships.di

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.pixelarts.spacexships.factory.MainViewModelFactory
import com.pixelarts.spacexships.ui.MainViewModel
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {
    @Provides
    @ActivityScope
    fun providesMainViewModel(factory: MainViewModelFactory): MainViewModel = ViewModelProviders.of(activity, factory)
        .get(MainViewModel::class.java)
}