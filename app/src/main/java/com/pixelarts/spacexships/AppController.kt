package com.pixelarts.spacexships

import android.app.Application
import com.pixelarts.spacexships.di.AppComponent
import com.pixelarts.spacexships.di.AppModule
import com.pixelarts.spacexships.di.DaggerAppComponent
import com.pixelarts.spacexships.di.NetworkModule

class AppController:Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule())
            .networkModule(NetworkModule())
            .build()
    }
}